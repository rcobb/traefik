local traefik = import '../../traefik.libsonnet';
local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local envVar = k.core.v1.envVar;
local secret = k.core.v1.secret;
local volume = k.core.v1.volume;
local volumeMount = k.core.v1.volumeMount;

local digitalOceanToken = {
  secretKey: 'digitaloceanToken',
  secretName: 'digitalocean-token',
  value: std.extVar('digitalocean-token'),
};

local config = {
  namespace: 'default',
  dashboard+: {
    expose: true,
    hostname: 'tf.k3d.maczukin.net',
    auth: {
      dashboard: 'dashboard:$2y$05$Qo61wZBZbdOIKEg5UJd5xO/DOBPl51votp5mh.UVHGWxFEga0TWMi',  // password: dashboard-pass
      prometheus: 'prometheus:$2y$05$uuzfQF..TjyRyTCTb/q1buWhaj5Q/dMoUGLdCXilsfym6RYV2qZsu',  // password: prometheus-pass
      ping: 'ping:$2y$05$EFHa7B1xIVYR.Mixwl9wNekjvvpC5hUUYs9YPRmQ6mAqCZfow.LmO',  // password: ping-pass
    },
  },
  acme+: {
    enabled: true,
    email: 'admin@maczukin.net',
    caserver: 'https://acme-staging-v02.api.letsencrypt.org/directory',

    storage+: {
      pvc+: {
        name: 'acme-storage-pvc',
        storageClassName: 'local-path',
      },
    },

    dnsChallenge+: {
      enabled: true,
      provider: 'digitalocean',
      resolvers: ['ns1.digitalocean.com:53', 'ns2.digitalocean.com:53', 'ns3.digitalocean.com:53'],
    },
  },
  envs: [
    local skr = envVar.valueFrom.secretKeyRef;
    envVar.withName('DO_AUTH_TOKEN') +
    skr.withName(digitalOceanToken.secretName) +
    skr.withKey(digitalOceanToken.secretKey),
  ],
};

traefik(config) +
{
  digitaloceanTokenSecret:
    secret.new(
      digitalOceanToken.secretName,
      {
        [digitalOceanToken.secretKey]: std.base64(digitalOceanToken.value),
      }
    ) +
    secret.metadata.withNamespace(config.namespace),
}
