function(params={})
  {
    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'ServiceMonitor',
    metadata: {
      name: 'traefik',
      namespace: params.namespace,
      labels: params.labels,
    },
    spec: {
      selector: {
        matchLabels:
          params.labels
          {
            'app.kubernetes.io/instance': 'traefik-internal',
          },
      },
      endpoints: [{
        port: 'traefik',
        relabelings: [{
          sourceLabels: ['namespace', 'pod'],
          separator: '/',
          targetLabel: 'instance',
        }],
      }],
    },
  }
