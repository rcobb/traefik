local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local containerPort = k.core.v1.containerPort;
local service = k.core.v1.service;
local servicePort = k.core.v1.servicePort;

local redirectArgs(config) =
  local redirections = config.redirections;
  if redirections != null then

    assert std.objectHas(redirections, 'entrypoint');

    local entrypoint = redirections.entrypoint;
    assert std.objectHas(entrypoint, 'to');

    local prefix = '--entrypoints.%s.http.redirections.entrypoint' % config.name;

    std.flattenArrays(
      std.prune([
        ['%s.to=%s' % [prefix, entrypoint.to]],
        if std.objectHas(entrypoint, 'scheme') then ['%s.scheme=%s' % [prefix, entrypoint.scheme]],
        if std.objectHas(entrypoint, 'permanent') then ['%s.permanent=%v' % [prefix, entrypoint.permanent]],
        if std.objectHas(entrypoint, 'priority') then ['%s.priority=%d' % [prefix, entrypoint.priority]],
      ])
    )
  else
    []
;

{
  new(name, exposePort):
    {
      local s = self,

      _config:: {
        name: name,
        exposePort: exposePort,
        port: exposePort,
        protocol: 'TCP',
        exposeProtocol: false,
        certResolver: null,
        redirections: null,
        serviceOverride: {},
      },

      serviceName(config)::
        '%s-%s-%s' % [config.name, s._config.name, std.asciiLower(s._config.protocol)],

      service(config)::
        local name = s.serviceName(config);
        service.new(
          name=name,
          selector=config.labels,
          ports=[
            servicePort.newNamed(s._config.name, s._config.exposePort, s._config.port) +
            servicePort.withProtocol(s._config.protocol),
          ]
        ) +
        service.metadata.withLabels(
          config.labels {
            'app.kubernetes.io/instance': name,
          }
        ) +
        service.spec.withType('LoadBalancer') +
        s._config.serviceOverride,

      containerArgs()::
        [
          // address
          '--entrypoints.%s.address=:%d' % [s._config.name, s._config.port] +
          if s._config.exposeProtocol then
            '/%s' % std.asciiLower(s._config.protocol)
          else
            '',
          if s._config.certResolver != null then
            '--entrypoints.%s.http.tls.certresolver=%s' % [s._config.name, s._config.certResolver],
        ] +
        redirectArgs(s._config),

      containerPort()::
        containerPort.newNamed(s._config.port, s._config.name) +
        containerPort.withProtocol(s._config.protocol),
    },

  withPort(port): { _config+:: { port: port } },
  withProtocol(protocol): { _config+:: { protocol: protocol } },
  withExposeProtocol(exposeProtocol): { _config+:: { exposeProtocol: exposeProtocol } },
  withCertResolver(certResolver): { _config+:: { certResolver: certResolver } },
  withRedirections(redirections): { _config+:: { redirections: redirections } },
  withServiceOverride(serviceOverride): { _config+:: { serviceOverride: serviceOverride } },
}
