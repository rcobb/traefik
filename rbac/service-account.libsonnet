local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local serviceAccount = k.core.v1.serviceAccount;

local defaults = {
  namespace: error 'must provide namespace',
  serviceAccountName: error 'must provide serviceAccountName',
};

function(params={}) {
  local p = self,
  _config:: defaults + params,

  serviceAccount:
    serviceAccount.new(p._config.serviceAccountName) +
    serviceAccount.metadata.withLabels(p._config.labels) +
    serviceAccount.metadata.withNamespace(p._config.namespace),
}
