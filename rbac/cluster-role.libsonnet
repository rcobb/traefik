local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local clusterRole = k.rbac.v1.clusterRole;

local defaults = {
  serviceAccountName: error 'must provide serviceAccountName',
};

function(params={}) {
  local p = self,
  _config:: defaults + params,

  clusterRole:
    clusterRole.new(p._config.serviceAccountName) +
    clusterRole.metadata.withLabels(p._config.labels) +
    clusterRole.withRules([
      {
        apiGroups: [''],
        resources: ['services', 'endpoints', 'secrets'],
        verbs: ['get', 'list', 'watch'],
      },
      {
        apiGroups: ['extensions', 'networking.k8s.io'],
        resources: ['ingresses', 'ingressclasses'],
        verbs: ['get', 'list', 'watch'],
      },
      {
        apiGroups: ['extensions'],
        resources: ['ingresses/status'],
        verbs: ['update'],
      },
      {
        apiGroups: ['traefik.containo.us'],
        resources: [
          'ingressroutes',
          'ingressroutetcps',
          'ingressrouteudps',
          'middlewares',
          'middlewaretcps',
          'serverstransports',
          'tlsoptions',
          'tlsstores',
          'traefikservices',
        ],
        verbs: ['get', 'list', 'watch'],
      },
    ]),
}
