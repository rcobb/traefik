local tr = import './_traefik_resource.libsonnet';

{
  new(name): tr.new('Middleware', name),
  metadata: tr.metadata,
  withSpec(spec): { spec: spec },
  withSpecMixin(spec): { spec+: spec },
}
